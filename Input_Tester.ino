/*
 * Digital input tester for Arduino Uno and Nano (ATmega328P).
 * 
 * This sketch tests digital read on all the inputs pins on the arduino that work with
 * digitalRead(). That is digital pins 0 to 13 and analog pins A0 to A5.
 * 
 * Digital pin 1 is connected to TX (serial transmit) and is used for serial communication and so cannot be tested
 * while using serial comms.
 * 
 * Digital pin 13 is attached to the onboard LED which pull it down to about 1.7V and so
 * it will never read HIGH.
 * 
 * Analog pins A6 and A7 cannot be used for digital input.
 * 
 * To test:
 *  - Open the serial monitor with  baud rate 9600.  
 *  - All pins except 1 and 13 should read 1 (HIGH) when not connected.
 *  - All pins should read 0 (LOW) when connected to the GND pin.
 * 
 */

int analogPins[6] {A0, A1, A2, A3, A4, A5};

void setup() {
  // put your setup code here, to run once:

pinMode(0, INPUT_PULLUP);
pinMode(1, INPUT_PULLUP);
pinMode(2, INPUT_PULLUP);
pinMode(3, INPUT_PULLUP);
pinMode(4, INPUT_PULLUP);
pinMode(5, INPUT_PULLUP);
pinMode(6, INPUT_PULLUP);
pinMode(7, INPUT_PULLUP);
pinMode(8, INPUT_PULLUP);
pinMode(9, INPUT_PULLUP);
pinMode(10, INPUT_PULLUP);
pinMode(11, INPUT_PULLUP);
pinMode(12, INPUT_PULLUP);
pinMode(13, INPUT_PULLUP);

pinMode(A0, INPUT_PULLUP);
pinMode(A1, INPUT_PULLUP);
pinMode(A2, INPUT_PULLUP);
pinMode(A3, INPUT_PULLUP);
pinMode(A4, INPUT_PULLUP);
pinMode(A5, INPUT_PULLUP);
Serial.begin(9600);
}

  void loop() {
    // put your main code here, to run repeatedly:
  for(int i = 0; i <= 13; i++)
  {
    Serial.print("Pin ");
    Serial.print(i);
    Serial.print(": ");
    Serial.println(digitalRead(i));
  }
for(int i = 0; i <= 5; i++)
  {
    Serial.print("Pin ");
    String eye = String(i);
    String pinName = "A" + eye;
    Serial.print(pinName);
    Serial.print(": ");
    Serial.println(digitalRead(analogPins[i]));
  }
  Serial.println("---------------------------------------------------");
  delay(1000);
}
