# Arduino Digital Input Tester

This sketch tests all the digital inputs on ATmega328P based Arduinos (Uno, Nano).

Requires no external components just a jumper to ground the inputs while testing.



This sketch tests digital read on all the inputs pins on the arduino that work with

digitalRead(). That is digital pins 0 to 13 and analog pins A0 to A5.



Digital pin 1 is connected to TX (serial transmit) and is used for serial communication and so cannot be tested while using serial comms.



Digital pin 13 is attached to the onboard LED which pulls it down to about 1.7V and so

it will never read HIGH.



Analog pins A6 and A7 cannot be used for digital input.



To test:

* Open the serial monitor with  baud rate 9600.  
* All pins except 1 and 13 should read 1 (HIGH) when not connected.
* All pins should read 0 (LOW) when connected to the GND pin.
